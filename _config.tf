terraform {
  required_version = "~> 1.5.3"

  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.14"
    }
  }

  backend "local" {
    path = "./terraform.tfstate"
  }
}

provider "gitlab" {
  # token = var.token
}
