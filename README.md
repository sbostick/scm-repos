GitLab Structure
----------------

    Group
      Project
    Personal
      Project


Bringup
-------

[GitLab Access Token](https://gitlab.com/-/profile/personal_access_tokens)

```
export GITLAB_TOKEN=$(pass gitlab.com/coreinfra/terraform-access)
terraform apply
```


References
----------

* https://github.com/gitlabhq/terraform-provider-gitlab

* https://registry.terraform.io/providers/gitlabhq/gitlab/latest
* https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/group
* https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project

* https://docs.gitlab.com/ee/user/permissions.html

https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs
