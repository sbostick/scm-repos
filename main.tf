########################################################################
#                Personal projects (git repositories)                  #
########################################################################

locals {
  default_repo_manifest = {
    "hello" = { "description" = "Managed by Terraform" },
  }
}

data "gitlab_user" "sbostick" {
  username = "sbostick"
}

resource "gitlab_project" "default" {
  for_each     = local.default_repo_manifest
  name         = each.key
  description  = each.value.description

  # Omitting the namespace_id works fine when creating new repos under a user
  # private namespace. The documentation shows usage of "data.gitlab_user"
  # to lookup and specify the namespace id explicitly but subsequent terraform
  # 'plan' operations pepetually show namespace_id transition to zero. Confirmed
  # that the user data source is returning a zero namespace_id. But simply not
  # setting it at all works fine for all CRUD operations on the project (repo)
  # resource.

  # namespace_id = data.gitlab_user.sbostick.namespace_id

  lifecycle {
    prevent_destroy = true
  }
}

output "default_ssh_url" {
  value = {for k, v in gitlab_project.default: k => v.ssh_url_to_repo }
}

output "default_web_url" {
  value = {for k, v in gitlab_project.default: k => v.web_url }
}

output "default_http_url" {
  value = {for k, v in gitlab_project.default: k => v.http_url_to_repo }
}

output "user" {
  value = data.gitlab_user.sbostick
}
