# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.14.0"
  constraints = "~> 3.14"
  hashes = [
    "h1:xR1R5xT0jLXzRqqkHLh8fQ6MjHSwvw1qajFulebHHlY=",
  ]
}
